import React, {Component} from 'react'
import {StyleSheet, Text, View} from 'react-native'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import logger from 'redux-logger'
import createSagaMiddleware from 'redux-saga'

import rootReducer from './src/app/rootReducer'
import rootSaga from './src/app/saga/saga'
import AppNavigator from './src/app/navigator'

const sagaMiddleware = createSagaMiddleware()

const store = createStore(
  rootReducer,
  applyMiddleware(sagaMiddleware, logger),
)
sagaMiddleware.run(rootSaga)

export default class App extends Component{
  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <AppNavigator />
        </View>
      </Provider>
      
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})
