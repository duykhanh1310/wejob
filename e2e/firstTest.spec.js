describe('WeJob', () => {
  beforeEach(async () => {
    await device.reloadReactNative()
  })

  it('Should show avatar', async () => {
    await expect(element(by.id('Avatar'))).toBeVisible()
  })

  it('should have JobList Screen', async () => {
    await expect(element(by.id('JobList'))).toBeVisible()
  })

  it('should show Profile after tap', async () => {
    await element(by.id('Navigate_Profile')).tap()
    await expect(element(by.id('Profile'))).toBeVisible()
  })
})