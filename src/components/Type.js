import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import Constant from '../utils/Constant'

const Type = props => {
  const { type, style } = props
  return (
    <View style={[styles.typeWrapper, style]}>
      <Text style={styles.type}>{type}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  typeWrapper: {
    backgroundColor: Constant.colors.lightGrey_3,
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 4,
    borderColor: Constant.colors.lightGrey_3,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 15,
    marginTop: 10,
    height: 25,
    paddingLeft: 8,
    paddingRight: 8,
  },
  type: {
    color: Constant.colors.blue,
    fontWeight: '600',
  },
})

export default Type
