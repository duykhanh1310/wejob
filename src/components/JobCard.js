import React from 'react'
import {
  View,
  Text,
  StyleSheet
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import Constant from '../utils/Constant'

export const JobCard = props => {
  const {
    code,
    title,
    department,
    address,
    type,

  } = props
  return (
    <View style={styles.jobsWrapper}>
      <View style={styles.leftCol}/>
      <View style={styles.titleWarpper}>
        <Text style={styles.jobCode}>{code}</Text>
        <Text style={styles.jobTitle}>{title}</Text>
        <Icon
          name='ios-warning'
          color={Constant.colors.red}
          size={18}

        />
      </View>
      <View style={styles.jobShortDescriptionWrapper}>
        <Text style={styles.jobDuration}>9:30am - 12:30pm</Text>

        <View style={styles.typeDeptWrapper}>
          <Text style={styles.jobDept}>{department}</Text>
          <View style={styles.typeWrapper}>
            <Text style={styles.type}>{type}</Text>
          </View>
        </View>

        <View style={styles.addressDispatchWrapper}>
          <Text
            numberOfLines={2}
            style={styles.address}
          >{`${address.street}, ${address.city}, ${address.cityCode}`}</Text>

          <View style={styles.dispatchWrapper}>
            <Text style={styles.dispatch}>Dispatched </Text>
          </View>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  jobsWrapper: {
    marginTop: 5,
    marginRight: 10,
    marginLeft: 10,
    borderStyle: 'solid',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    backgroundColor: Constant.colors.white,
    flexWrap: 'wrap',
    height: 180,
    shadowColor: 'rgba(0, 0, 0, 0.2)',
    shadowOffset: {
      width: 1,
      height: 1.5
    },
    shadowOpacity: 0.2,
    elevation: 2,
  },
  leftCol: {
    borderStyle: 'solid',
    borderLeftWidth: 7,
    borderLeftColor: Constant.colors.orange,
    height: 175,
  },
  titleWarpper: {
    padding: 10,
    marginRight: 10,
    marginBottom: 10,
    borderStyle: 'solid',
    borderBottomWidth: 1,
    borderBottomColor: Constant.colors.lightGrey_1,
    width: '90%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  jobTitle: {
    marginLeft: 15,
    color: Constant.colors.lightGrey_2,
    fontWeight: '500',
    letterSpacing: 0.2,
    width: '70%',
  },
  jobCode: {
    color: Constant.colors.lightGrey,
    fontWeight: '700',
    fontSize: 15,
  },
  jobShortDescriptionWrapper: {
    marginLeft: 10,
  },
  jobDuration: {
    marginBottom: 10,
    fontWeight: '500',
    color: Constant.colors.lightGrey_2
  },
  jobDept: {
    marginBottom: 10,
    fontWeight: '600',
    color: Constant.colors.lightGrey_2,
    fontSize: 15,
    letterSpacing: 0.3,
  },
  addressDispatchWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  dispatchWrapper: {
    borderStyle: 'solid',
    borderColor: Constant.colors.orange,
    borderWidth: 1,
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
    height: 25,
    paddingLeft: 3,
    paddingRight: 3,
    marginLeft: 10,
    marginTop: 10,
  },
  dispatch: {
    color: Constant.colors.orange,
    fontWeight: '600'
  },
  address: {
    color: Constant.colors.lightGrey_2,
    width: '60%'
  },
  typeWrapper: {
    backgroundColor: Constant.colors.lightGrey_3,
    justifyContent: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 5,
  },
  type: {
    color: Constant.colors.blue,
    fontWeight: '600',
  },
  typeDeptWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
})