import React, { Component } from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'
import ArrowIcon from 'react-native-vector-icons/Feather'
import EditIcon from 'react-native-vector-icons/Feather'
import Constant from '../utils/Constant'

export default class Header extends Component {
  
  _onRenderBackIcon() {
    const { backIcon, navigation, rightBtn, closeModal } = this.props
    if (backIcon) {
      return (
        <ArrowIcon 
          name='chevron-left'
          size={30}
          onPress={() => rightBtn ? closeModal() : navigation.goBack()}
          color={Constant.colors.white}
          style={styles.headerIcon}
        />
      )
    }
  }
  render() {
    const { children, style, backIcon, rightBtn, onPress, edit } = this.props
    return (
      <View style={[styles.header, style ]}>
        {this._onRenderBackIcon()}
        <Text style={styles.headerText}>
          {children}
        </Text>
        {backIcon && !rightBtn && !edit ? <View style={{ marginLeft: 40}}/> : null}
        {rightBtn ? (
          <TouchableOpacity
            onPress={onPress}
          >
            <Text style={[styles.headerText, { marginRight: 10 }]}>Share</Text>
          </TouchableOpacity>
        ) : null}

        {edit ? (
          <TouchableOpacity
            onPress={onPress}
            style={{ marginRight: 10, marginTop: 15}}
          >
            <EditIcon
              name='edit'
              color={Constant.colors.white}
              size={20}

            />
          </TouchableOpacity>
        ) : null}
      </View>
    )
  }
}
const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: 50 + Constant.layout.navPadding,
    backgroundColor: Constant.colors.primary,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    color: Constant.colors.white,
    fontSize: 18,
    fontWeight: '700',
    marginTop:  10,
  },
  headerIcon: {
    marginLeft: 5,
    paddingTop: 10,
  },
})
