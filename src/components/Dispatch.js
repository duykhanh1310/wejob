import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import Constant from '../utils/Constant'

const Dispatch = props => {
  const { isDispatch, style } = props
  return (
    <View style={[styles.dispatchWrapper, style]}>
      <Text style={styles.dispatch}>{isDispatch}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  dispatchWrapper: {
    borderStyle: 'solid',
    borderColor: Constant.colors.orange,
    borderWidth: 1,
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
    height: 25,
    paddingLeft: 3,
    paddingRight: 3,
    marginTop: 10,
  },
  dispatch: {
    color: Constant.colors.orange,
    fontWeight: '600'
  }
})

export default Dispatch
