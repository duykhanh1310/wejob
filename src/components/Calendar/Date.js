import React, { PureComponent } from 'react'
import {
  Text,
  View,
  TouchableOpacity,
} from 'react-native'
import type Moment from 'moment'
import Constant from '../../utils/Constant'

export default class Date extends PureComponent {

  props: {
    // Date to render
    date: Moment,
    // Index for `onPress` and `onRender` callbacks
    index: number,
    // Whether it's the currently selected date or no
    isActive: boolean,
    // Called when user taps a date
    onPress: (index: number) => void,
    // Called after date is rendered to pass its width up to the parent component
    onRender: (index: number, width: number) => void,
  };

  // Style helper functions that merge active date styles with the default ones
  // when rendering a date that was selected by user or was set active by default

  getContainerStyle = () => ({
    ...styles.container,
    ...(this.props.isActive ? styles.containerActive : {})
  });

  getDayStyle = () => ({
    ...styles.text,
    ...styles.day,
    ...(this.props.isActive ? styles.textActive : {})
  });

  getDateStyle = () => ({
    ...styles.text,
    ...styles.date,
    ...(this.props.isActive ? styles.dateActive : {})
  });

  getDateViewStyle = () => ({
    ...styles.dateView,
    ...(this.props.isActive ? styles.dayViewActive: styles.dayViewInactive)
  })

  // Call `onRender` and pass component's with when rendered
  onLayout = (event: { nativeEvent: { layout: { x: number, y: number, width: number, height: number } } }) => {
    const {
      index,
      onRender,
    } = this.props
    const { nativeEvent: { layout: { width } } } = event
    onRender(index, width)
  };

  // Call `onPress` passed from the parent component when date is pressed
  onPress = () => {
    const { index, onPress } = this.props
    onPress(index)
  };

  render() {
    const { date } = this.props
    return (
      <TouchableOpacity
        style={this.getContainerStyle()}
        onLayout={this.onLayout}
        onPress={this.onPress}
      >
        <Text style={this.getDayStyle()}>{date.format('ddd').toUpperCase()}</Text>
        <View style={this.getDateViewStyle()}>
          <Text style={this.getDateStyle()}>{date.format('DD')}</Text>
        </View>
      </TouchableOpacity>
    )
  }

}

const styles = {
  container: {
    borderBottomColor: 'transparent',
    borderBottomWidth: 2,
    paddingHorizontal: 15,

  },
  containerActive: {

  },
  day: {
    fontSize: 12,
    marginTop: 15,
  },
  date: {
    fontSize: 18,

  },
  text: {
    textAlign: 'center',
  },
  textActive: {
    color: Constant.colors.black,
  },
  dateActive: {
    color: Constant.colors.white
  },
  dateView: {
    marginTop: 20,
  },
  dayViewActive: {
    backgroundColor: Constant.colors.primary,
    padding: 8,
    borderRadius: 3
  },
  dayViewInactive: {
    paddingTop: 8,
  }
}