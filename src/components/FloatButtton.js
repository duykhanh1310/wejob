import React, { Component } from 'react'
import { StyleSheet, TouchableHighlight } from 'react-native'

import Constant from '../utils/Constant'

export default class FloatButton extends Component  {
  _getDeviceName() {
    const { deviceName } = this.props
    return deviceName === 'iPhone 5s' || deviceName ==='iPhone 5'
  }
  render() {
    const {
      children,
      style,
      onPress,
      disable,
    } = this.props
    return (
      <TouchableHighlight
        style={[
          styles.floaten,
          style,
        ]}
        onPress={onPress}
        disabled={disable}
        activeOpacity={1}
      >
        {children}
      </TouchableHighlight>
    )
  }
  
}

const styles = StyleSheet.create({
  floaten: {
    position: 'absolute',
    bottom: 20,
    right: 20,
    zIndex: 999,
    shadowColor: Constant.colors.black,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: .3,
    elevation: 2,
    backgroundColor: Constant.colors.primary,
    borderRadius: 25,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: 45,
    height: 45,
  },
})