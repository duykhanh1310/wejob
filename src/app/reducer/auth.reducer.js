import {
  LOG_IN_USER_REQUEST,
  CHECK_USER_LOGIN,
  LOG_IN_USER_RESPONSE,
  AUTH_STATE
} from '../types/auth.types'

const INITIAL_STATE = {
  isUserLogin: false,
  isLogin: false,
  isLoginSuccess: false,
  isLogout: false,
  isUserLogoutSuccess: false,
  isCreateUser: false,
  userInfo: null,
  token: null,
  isError: false,
}

const authReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case AUTH_STATE:
      return {
        ...state,
      }
    case LOG_IN_USER_REQUEST:
      return {
        ...state,
        isLogin: action.payload,
        isUserLogin: false,
      }
    case CHECK_USER_LOGIN:
      return {
        ...state,
        isUserLogin: action.payload
      }
    case LOG_IN_USER_RESPONSE:      
      return {
        ...state,
        isLogin: false,
        isUserLogin: true,
        userInfo: action.payload
      }
    default:
      return state
  }
}

export default authReducer