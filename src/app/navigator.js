import {
  createAppContainer,
  createStackNavigator,
  createSwitchNavigator,
} from 'react-navigation'

import JobListScreen from '../views/JobList/index'
import JobDetailScreen from '../views/JobDetail/index'
import JobScreen from '../views/Jobs/index'
import Profilecreen from '../views/Profile/index'

import AuthLoadingScreen from './AuthLoading'
import AuthScreen from '../views/Auth/index'

const AuthStack = createStackNavigator({
  Auth: AuthScreen,
}, {
  initialRouteName: 'Auth',
  headerMode: 'none',
})

const AppStack = createStackNavigator({
  JobList: JobListScreen,
  JobDetail: JobDetailScreen,
  Jobs: JobScreen,
  Profile: Profilecreen
}, {
  initialRouteName: 'JobList',
  headerMode: 'none'
})

const StackNavigation = createSwitchNavigator({
  AuthLoading: AuthLoadingScreen,
  Auth: AuthStack,
  App: AppStack,
}, {
  initialRouteName: 'AuthLoading'
})

export default createAppContainer(StackNavigation)
