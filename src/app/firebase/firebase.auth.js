import firebase from 'react-native-firebase'
import FBSDK from 'react-native-fbsdk'
import { GoogleSignin } from 'react-native-google-signin'

const { LoginManager, AccessToken} = FBSDK

const auth = firebase.auth()

// Check user status
export function onAuthStateChanged() {
  return new Promise((resolve, reject) => {
    auth.onAuthStateChanged(user => {
      if (user) {
        resolve(user)
      } else {
        reject(new Error('There is no user'))
      }
    })
  })
}

// Login with facebook
export function facebookLogin() {
  return new Promise((resolve, reject) => {
    LoginManager.logInWithReadPermissions(['public_profile'])
      .then(FBloginResult => {
        if (FBloginResult.isCancelled) {
          throw new Error('Login cancelled')
        }

        if (FBloginResult.deniedPermissions) {
          throw new Error('We need the requested permissions')
        }
      })
      .then(_ => {
        AccessToken.getCurrentAccessToken()
          .then(data => {
            auth.signInWithCredential(
              firebase.auth.FacebookAuthProvider.credential(data.accessToken)
            )
              .then(user => resolve(user))
              .catch(err => reject(err))
          })
      })
      .catch(error => {
        reject(error)
      })
  })
}

// Login with google
export function googleLogin() {
  return new Promise((resolve, reject) => {
    GoogleSignin.configure()
    GoogleSignin.signIn().then(userResult => {
      const credential = firebase.auth.GoogleAuthProvider.credential(
        userResult.idToken,
        userResult.accessToken
      )
      auth.signInWithCredential(credential)
        .then(user => {
          if (user) {
            resolve(user)
          } else {
            reject(new Error('Login with google failure'))
          }
        })
    })

  })
}