
import { takeLatest, put, call, all } from 'redux-saga/effects'
import { fireErr } from '../../utils/SlackMessage'
import {
  AUTH_STATE,
  logInUserRequest,
  checkUserLogin,
  logInUserResponse,
  LOG_IN_USER_RESPONSE,
  FACEBOOK_LOGIN,
  GOOGLE_LOGIN,
} from '../types/auth.types'
import { onAuthStateChanged, facebookLogin, logOut, googleLogin } from '../firebase/firebase.auth'

/**
|--------------------------------------------------
| CHECK USER LOGIN
|--------------------------------------------------
*/
function* checkIfUserLogin() {
  try {
    yield put(logInUserRequest(true))
    const user = yield call(onAuthStateChanged)
    yield put(checkUserLogin(true))
    yield put(logInUserResponse(user))
  } catch (error) {
    fireErr(error)
  }
} 

/**
|--------------------------------------------------
| LOGIN USING FACEBOOK
|--------------------------------------------------
*/
function* loginWithFacebook() {
  try {
    yield put(logInUserRequest(true))
    const fbResponse = yield call(facebookLogin)
    if (fbResponse) {
      yield put(logInUserResponse(fbResponse))
    }
   
  } catch (error) {
    fireErr(error)
  }
}

/**
|--------------------------------------------------
| LOGIN USING GOOGLE
|--------------------------------------------------
*/
function* loginWithGoogle() {
  try {
    yield put(logInUserRequest(true))
    const ggResponse = yield call(googleLogin)
    if (ggResponse) {
      yield put(logInUserResponse(ggResponse))
    } 
  } catch (error) {
    fireErr(error)
  }
}

function* actionWatcher() {
  yield takeLatest(AUTH_STATE, checkIfUserLogin)
  yield takeLatest(FACEBOOK_LOGIN, loginWithFacebook)
  yield takeLatest(GOOGLE_LOGIN, loginWithGoogle)
}

export default function* rootSaga() {
  yield all([
    actionWatcher(),
  ])
}
