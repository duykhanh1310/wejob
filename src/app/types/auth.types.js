
import { createAction } from 'redux-actions'

export const CHECK_USER_LOGIN = 'CHECK_USER_LOGIN'
export const checkUserLogin = createAction(CHECK_USER_LOGIN)
export const LOG_IN_USER_REQUEST = 'LOG_IN_USER_REQUEST'
export const logInUserRequest = createAction(LOG_IN_USER_REQUEST)
export const LOG_IN_USER_RESPONSE = 'LOG_IN_USER_RESPONSE'
export const logInUserResponse = createAction(LOG_IN_USER_RESPONSE)

export const AUTH_STATE = 'AUTH_STATE'
export const FACEBOOK_LOGIN = 'FACEBOOK_LOGIN'
export const GOOGLE_LOGIN = 'GOOGLE_LOGIN'