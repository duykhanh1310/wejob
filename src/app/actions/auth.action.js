import { AUTH_STATE, FACEBOOK_LOGIN, GOOGLE_LOGIN } from '../types/auth.types'

export const getAuthState = _ => ({
  type: AUTH_STATE
})

export const loginUsingFacebook = _ => ({
  type: FACEBOOK_LOGIN
})

export const loginUsingGoogle = _ => ({
  type: GOOGLE_LOGIN
})