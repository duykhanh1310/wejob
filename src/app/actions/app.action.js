export const ACTION_TYPES = {
  INIT_APP: 'app/init_app',
  GET_SINGLE_JOB: 'job/GET_SINGLE_JOB',
}
    
const initialState = {
  initApp: true,
  singleJob: null,
}
    
export default (state = initialState, action) => {
  switch (action.type) {
    case ACTION_TYPES.GET_PET_TYPE:
      return { ...state, initApp: action.payload }
    case ACTION_TYPES.GET_SINGLE_JOB:
      return { ...state, singleJob: action.payload }
    default:
      return state
  }
}

export const initApp = type => ({
  type: ACTION_TYPES.INIT_APP,
  payload: type,
})

export const getSingleJob = (job, index) => ({
  type: ACTION_TYPES.GET_SINGLE_JOB,
  payload: {
    job,
    index
  }
})