import { combineReducers } from 'redux'

import AppReducer from '../app/actions/app.action'
import AuthReducer from './reducer/auth.reducer'

const rootReducer = combineReducers({
  app: AppReducer,
  auth: AuthReducer
})

export default rootReducer