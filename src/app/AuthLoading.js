import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  Animated,
  Easing,
} from 'react-native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import LottieView from 'lottie-react-native'

import { getAuthState } from '../app/actions/auth.action'
import Constant from '../utils/Constant'

class AuthLoading extends Component {
  constructor(props) {
    super(props)
    this.state = {
      progress: new Animated.Value(0),
    }
  }

  componentDidMount() {
    this.props.getAuthState()
    this.setupAnimation()
  }

  setupAnimation = () => {
    Animated.timing(this.state.progress, {
      toValue: 1,
      duration: 1000,
      easing: Easing.linear,
    }).start(() => {
      this.setState({
        progress: new Animated.Value(0),
      }, () => this.setupAnimation())
    })
  }
  
  UNSAFE_componentWillReceiveProps(nextProps) {
    const isUserLogin = nextProps.auth.isUserLogin
    this.props.navigation.navigate(isUserLogin ? 'App' : 'Auth')
  }

  render() {
    return (
      <View style={styles.container}>
        <LottieView
          source={require('../assets/Lottie/loading.json')}
          progress={this.state.progress}
          loop={true}
          autoPlay
          style={styles.loadingIndicator}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1 ,
    backgroundColor: Constant.colors.primary,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loadingIndicator: {
    height: '20%',
    width: '100%',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: Constant.layout.screenHeight/1.6,
    alignSelf: 'center',
  },
})

const mapStateToProps = state => {
  return {
    auth: state.auth,
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getAuthState,
    },
    dispatch
  )
}

export default connect(mapStateToProps,mapDispatchToProps)(AuthLoading)