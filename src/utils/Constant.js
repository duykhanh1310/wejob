// Dimensions
import { Platform, Dimensions } from 'react-native'

export const isIphoneX = () => {
  const dimen = Dimensions.get('window')
  return (
    Platform.OS === 'ios' &&
    !Platform.isPad &&
    !Platform.isTVOS &&
    (dimen.height === 812 || dimen.width === 812)
  )
}

const getNavPadding = () => {
  if (isIphoneX()) {
    return 40
  } else if (Platform.OS === 'ios') {
    return 20
  }
  return 0
}

export default {
  colors: {
    primary: '#1791FF',
    black: '#000000',
    white: '#FFFFFF',
    lightGrey: '#9B9B9B',
    lightGrey_1: '#EBEBEB',
    lightGrey_2: '#595A5B',
    lightGrey_3: '#E8E9EA',
    darkGray: '#4a4a4a',
    blue: '#002241',
    orange: 'orange',
    red: '#F3232E',
    facebook: '#4267B2',
    google: '#EB4535',
  },
  layout: {
    screenWidth: Dimensions.get('window').width,
    screenHeight: Dimensions.get('window').height,
    navPadding: getNavPadding(),
  },
  fonts: {
    fontFamily: 'Avenir',
  },
  host: {
    requestTimeout: 5000,
    deadline: 10000,
    retry: 3,
  },
}