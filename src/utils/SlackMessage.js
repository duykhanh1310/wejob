import Slack from 'react-native-slack-webhook'

const Config = require('../app/configure/development.json')

export const fireErr = errMess => {
  new Slack(Config.webHookUrl).post(String(errMess), '#bugs')
}