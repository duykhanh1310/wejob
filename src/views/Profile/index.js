import React, { Component } from 'react'
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {
  NavigationActions,
} from 'react-navigation'

import Header from '../../components/Header'
import Constant from '../../utils/Constant'

import firebase from 'react-native-firebase'
import { fireErr } from '../../utils/SlackMessage'

class Profile extends Component {
  _onSignUserOut() {
    firebase.auth().signOut().then(_ => {
      this.props.navigation.dispatch(
        NavigationActions.navigate({ routeName: 'Auth' })
      )
    }).catch(err => fireErr(err))
  }

  render() {
    return (
      <View
        style={styles.container}
        testID='Profile'
      >
        <Header
          backIcon
          style={styles.headerStyle}
          navigation={this.props.navigation}
        >
          Profile
        </Header>
        <TouchableOpacity
          style={styles.logOutWrapper}
          onPress={_ => this._onSignUserOut()}
        >
          <Icon
            name='exit-run'
            size={35}
            color={Constant.colors.white}
          />
          <Text style={styles.logOut}>Logout</Text>

        </TouchableOpacity>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  headerStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  logOutWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
    paddingLeft: 10,
    paddingBottom: 10,
    paddingTop: 10,
    borderStyle: 'solid',
    borderBottomColor: Constant.colors.red,
    borderBottomWidth: 0.3,
    borderTopColor: Constant.colors.red,
    borderTopWidth: 0.3,
    backgroundColor: Constant.colors.red
  },
  logOut: {
    marginLeft: 10,
    fontSize: 18,
    color: Constant.colors.white,
    fontWeight: '600'
  }
})

const mapStateToProps = state => ({
  auth: state.auth,
})

export default connect(mapStateToProps, null)(Profile)