import React, { Component } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  Animated
} from 'react-native'
import { connect } from 'react-redux'
import FastImage from 'react-native-fast-image'

import Constant from '../../utils/Constant'
import Deck from './Deck'
import { JobCard } from '../../components/JobCard'

class Jobs extends Component {
  scrollX = new Animated.Value(0)
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      isReloadJob: false,
      isSuffle: true,
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { singleJob } = nextProps.app
    if (singleJob !== null && singleJob.index === 6) {
      this.setState({ isReloadJob: false, isSuffle: true })
    }
  }
  _onRenderCard(item, index) {
    const {
      code,
      title,
      department,
      address,
      type,
    } = item
    return (
      <JobCard
        key={index}
        code={code}
        title={title}
        department={department}
        address={address}
        type={type}
      />
    )
  }
  _onRenderNoMoreCards() {
    return (
      <View style={styles.warningWrapper}>
        <Text style={styles.warning}>That's all job for today. Reload to view again</Text>
        <TouchableOpacity
          style={styles.reloadWrapper}
          onPress={_ => this.setState({ isReloadJob: true }, _ => {
            this.setState({ isSuffle: false })
          })}
        >
          <Text style={styles.reload}>Reload</Text>
        </TouchableOpacity>
      </View>
    )
  }

  _onRenderSingleJob() {
    const { singleJob } = this.props.app
    if (singleJob !== null) {
      const {
        code,
        title,
        department,
        address,
        type,
      } = singleJob.job
      return (
        <TouchableOpacity
          style={styles.singleJobWrapper}
          onPress={_ => this.props.navigation.navigate('JobDetail', { jobInfomation: {
            item: singleJob.job
          }})}
        >
          <JobCard
            code={code}
            title={title}
            department={department}
            address={address}
            type={type}
          />
        </TouchableOpacity>
       
      )
    }
  }

  render() {
    const { jobs } = this.props
    const { isLoading } = this.state
    const position = Animated.divide(this.scrollX, Constant.layout.screenWidth)
    return (
      <ScrollView
        style={styles.container}
      >
        <View>
          <ScrollView
            horizontal  
            pagingEnabled
            showsHorizontalScrollIndicator={false}
            ref={myScrollView => (this.myScrollView = myScrollView)}
            bounces
            scrollEventThrottle={16}
            onScroll={Animated.event([
              { nativeEvent: { contentOffset: { x: this.scrollX } } },
            ])}
            style={{ marginTop: 15 }}
          >
            {
              jobs.map((job, index) => {
                return (
                  <View key={index}>
                    <FastImage
                      source={{ uri: job.image }}
                      style={styles.bannerImg}
                      resizeMode={FastImage.resizeMode.cover}
                      onLoadStart={_ => this.setState({ isLoading: true })}
                      onLoadEnd={_ => this.setState({ isLoading: false })}
                    />
               
                  </View>
                )
              })
            }
          </ScrollView>
          {
            !isLoading && (
              <View style={styles.dotWrapper}>
                {jobs.map((_, i) => {
                  const opacity = position.interpolate({
                    inputRange: [i - 1, i, i + 1],
                    outputRange: [0.2, 1, 0.2],
                    extrapolate: 'clamp',
                  })
                  return <Animated.View key={i} style={[{ opacity }, styles.dot]} />
                })}
              </View>
            )
          }
        </View>
        <View style={styles.deckWrapper}>
          <Deck
            data={jobs}
            renderCard={this._onRenderCard.bind(this)}
            renderNoMoreCards={this._onRenderNoMoreCards.bind(this)}
            isReloadDeck={this.props.isSelectedDate || (this.state.isReloadJob &&
              this.state.isSuffle)}
          />

          {this._onRenderSingleJob()}

        </View>
        
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  bannerImg: {
    width: Constant.layout.screenWidth,
    height: 180,
  },
  dotWrapper: {
    position: 'absolute',
    bottom: 5,
    right: 0,
    left: 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  dot: {
    height: 8,
    width: 8,
    backgroundColor: Constant.colors.primary,
    margin: 8,
    borderRadius: 5,
    alignSelf: 'center',
  },

  warningWrapper: {
    marginTop: 10,
  },
  warning: {
    textAlign: 'center',
    marginBottom: 10,
    fontSize: 16
  },
  deckWrapper: {
    marginTop: 15,
  },
  singleJobWrapper: {
    marginTop: 20,
    marginBottom: 30,
  },
  reloadWrapper: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  reload: {
    color: Constant.colors.primary,
    fontSize: 16,
    fontWeight: '600'
  },
})

const mapStateToProps = state => {
  return {
    app: state.app,
  }
}
export default connect(mapStateToProps, null)(Jobs)