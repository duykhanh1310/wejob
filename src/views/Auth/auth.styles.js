import { StyleSheet } from 'react-native'
import Constant from '../../utils/Constant'

export const styles = StyleSheet .create({
  container: {
    alignItems: 'center',
    flex: 1,
    backgroundColor: Constant.colors.primary,
    justifyContent: 'center'
  },
  appName: {
    fontSize: 40,
    color: Constant.colors.white,
  },
  signInWrapper: {
    marginTop: 30,
    width: '80%',
    backgroundColor: Constant.colors.white,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 15,
    paddingBottom: 15,
    borderRadius: 2,
    shadowColor: Constant.colors.shadow,
    shadowOffset: {
      width: 1,
      height: 2,
    },
    shadowOpacity: .3,
    elevation: 2,
  },
  signInText: {
    color: Constant.colors.primary,
    fontWeight: '600',
    fontSize: 16,
  },

  loginBtnWrapper: {
    borderRadius: 2,
  },
  logoIcon: {
    marginRight: 10,
  },
  wrapLine: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
  },
  line: {
    borderBottomColor: Constant.colors.white,
    borderBottomWidth: 1,
    alignSelf: 'center',
    width: '80%',
  },
  lineWhite: {
    paddingVertical: 5,
    borderBottomColor: Constant.colors.white,
    borderBottomWidth: 1,
    alignSelf: 'center',
    width: '100%',
  },
  text: {
    position: 'absolute',
    padding: 10,
    fontSize: 12,
    backgroundColor: Constant.colors.primary,
    color: Constant.colors.white,
    justifyContent: 'center',
  },
  socialWrapper:{
    width: '60%',
    marginTop: 30,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: Constant.colors.white,
    alignItems: 'center',
    paddingTop: 5,
    paddingBottom: 1,
    borderRadius: 2,
  },
  loadingIndicator: {
    height: '100%',
    width: '100%',
    alignSelf: 'center',
  },
})
  