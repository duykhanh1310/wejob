import React, { Component } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  Animated,
  Easing,
} from 'react-native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { NavigationActions } from 'react-navigation'
import Icon from 'react-native-vector-icons/Ionicons'
import Modal from 'react-native-modal'
import LottieView from 'lottie-react-native'
import Constant from '../../utils/Constant'
import { styles } from './auth.styles'

import { loginUsingFacebook, loginUsingGoogle } from '../../app/actions/auth.action'

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isVisibleModal: false,
      progress: new Animated.Value(0),
    }
  }
  componentDidMount() {
    this.setupAnimation()
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { isUserLogin, isLogin } = nextProps.auth
    if (isUserLogin) {
      this.props.navigation.navigate(
        NavigationActions.navigate({ routeName: 'App' })
      )
    }
    if (isLogin) {
      this.setState({ isVisibleModal: true })
    }
  }
 
  setupAnimation = () => {
    Animated.timing(this.state.progress, {
      toValue: 1,
      duration: 1000,
      easing: Easing.linear,
    }).start(() => {
      this.setState({
        progress: new Animated.Value(0),
      }, () => this.setupAnimation())
    })
  }
  
  onLogin = () => {
    const { username, password } = this.state
    const { login } = this.props

    login(username, password)
  }
  _onRenderLoadingModal() {
    return (
      <Modal
        isVisible={this.state.isVisibleModal}
        animationIn={'fadeIn'}
        backdropOpacity={0.6}
      >
         
        <LottieView
          source={require('../../assets/Lottie/animation-w600-h600.json')}
          progress={this.state.progress}
          loop={true}
          autoPlay
          style={styles.loadingIndicator}
        />
          
      </Modal>
    )
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.appName}>WE JOB</Text>
         
        <View style={styles.wrapLine}>
          <View style={styles.line} />
          <Text style={styles.text}>Login using</Text>
        </View>

        {/* Facebook Google Instagram*/}
        <View style={styles.socialWrapper}>
          {/* Facebook */}
          <TouchableOpacity
            style={styles.loginBtnWrapper}
            activeOpacity={0.5}
            onPress={_ => this.props.loginUsingFacebook()}
          >
            <Icon
              name='logo-facebook'
              color={Constant.colors.facebook}
              size={30}
              style={styles.logoIcon}
            />

          </TouchableOpacity>
          {/* Google */}
          <TouchableOpacity
            style={[styles.loginBtnWrapper]}
            activeOpacity={0.5}
            onPress={_ => this.props.loginUsingGoogle()}
          >
            <Icon
              name='logo-google'
              color={Constant.colors.google}
              size={30}
              style={styles.logoIcon}
            />
          </TouchableOpacity>    
        </View>
        {this._onRenderLoadingModal()}
      </View>
    )
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
})

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      loginUsingFacebook,
      loginUsingGoogle
    },
    dispatch
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)