import React, { Component } from 'react'
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity
} from 'react-native'
import { connect } from 'react-redux'
import FeatherIcon from 'react-native-vector-icons/Feather'
import MatIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import OtIcon from 'react-native-vector-icons/Octicons'
import moment from 'moment'
import faker from 'faker'
import type Moment from 'moment'
import FastImage from 'react-native-fast-image'

import Constant from '../../utils/Constant'
import FloatButton from '../../components/FloatButtton'
import Calendar from '../../components/Calendar/Calendar'
import Jobs from '../Jobs'

export type JobType = {
  date: Moment,
  code: string,
  title: string,
  description: string,
  image: string,
  location: string,
  contact: string,
  type: string,
  department: string,
};

const FAKE_JOBS: Array<JobType> = (_ => {
  const startDay = moment().subtract(5, 'days').startOf('day')

  return [...new Array(64)].map(_ => ({
    date: startDay.add(4, 'hours').clone(),
    code: `JOB-${faker.finance.account(4)}`,
    title: faker.lorem.sentence(7),
    description: faker.lorem.sentence(20),
    image: faker.image.city(Math.floor(Math.random() * 200) + 200, Math.floor(Math.random() * 200) + 150),
    address: {
      street: faker.address.streetAddress(true),
      city: faker.address.city(),
      cityCode: faker.address.zipCode(),
    },
    contact: {
      avatar: faker.internet.avatar(),
      name: faker.internet.userName(),
    },
    type: 'Installation',
    department: faker.commerce.department(),
    instruction: faker.lorem.sentence(4)
  }))
})()

const filterEvent = (date: Moment): ?Array<JobType> => 
  FAKE_JOBS.filter(event => event.date.isSame(date, 'day'))

class JobList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      jobs: filterEvent(moment()),
      isSelectDate: false,
      visibleTime: '',
    }
  }

  _onSelectDate = (date: Moment) => {
    this.setState({
      jobs: filterEvent(date),
      isSelectDate: true,
    })
  }

  _onRenderAvatar() {
    const { userInfo } = this.props.auth
    if (userInfo !== null && Object.keys(userInfo).length) {
      if (userInfo.user) {
        const { photoURL} = userInfo.user
        return (
          <TouchableOpacity
            onPress={_ => this.props.navigation.navigate('Profile')}
            testID='Navigate_Profile'
          >
            <FastImage
              source={{
                uri: photoURL,
              }}
              style={styles.avatar}
              resizeMode={FastImage.resizeMode.cover}
              testID='Avatar'
            />
          </TouchableOpacity>
        )
      } else {
        const { photoURL} = userInfo
        return (
          <TouchableOpacity
            onPress={_ => this.props.navigation.navigate('Profile')}
            testID='Navigate_Profile'
          >
            <FastImage
              source={{
                uri: photoURL,
              }}
              style={styles.avatar}
              resizeMode={FastImage.resizeMode.cover}
              testID='Avatar'
            />
          </TouchableOpacity>
        )
      }
      
    }
  }

  _onRenderHeader() {
    return (
      <View style={styles.headerContainer}>
        <View style={[styles.leftSide, { paddingBottom: 5}]}>
          <FeatherIcon
            name='menu'
            color={Constant.colors.white}
            size={22}
          />
          <Text style={styles.date}>{this.state.visibleTime}</Text>
        </View>

        <View style={[styles.leftSide, {
          paddingTop: 10,
        }]}>
          <MatIcon
            name='magnify'
            color={Constant.colors.white}
            size={22}
            style={{ marginTop: 5}}
          />
          <MatIcon
            name='bell'
            color={Constant.colors.white}
            size={20}
            style={styles.bellIcon}
          />
          <OtIcon
            name='settings'
            color={Constant.colors.white}
            size={19}
            style={{
              transform: [{ rotate: '90deg'}]
            }}
          />
          {this._onRenderAvatar()}
        </View>
      </View>
    )
  }
  _onGetVisibleMonthYear = data => {
    console.log(data)
    this.setState({ visibleTime: data })
  }
  _onRenderCalendar = _ => {
    return (
      <Calendar
        showDaysAfterCurrent={50}
        onSelectDate={this._onSelectDate}
        getVivisbleMonthYear={this._onGetVisibleMonthYear}
      />
    )
  }

  _onRenderImgs = _ => {

  }

  render() {
    const { jobs } = this.state
    return (
      <View style={styles.container} testID='JobList'>
        {this._onRenderHeader()}
        {this._onRenderCalendar()}
        {this._onRenderImgs()}
        <Jobs
          jobs={jobs}
          navigation={this.props.navigation}
          isSelectDate={this.state.isSelectDate}
        />
        <FloatButton>
          <OtIcon
            name='plus'
            color={Constant.colors.white}
            size={25}
          />
        </FloatButton>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Constant.colors.white,
  },
  headerContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    backgroundColor: Constant.colors.primary,
    height: 70,
    paddingLeft: 15,
    paddingRight: 10,
    paddingBottom: 10
  },
  date: {
    color: Constant.colors.white,
    marginLeft: 15,
    fontSize: 16,
    fontWeight: '600',
    alignSelf: 'center'
  },
  leftSide: {
    display: 'flex',
    flexDirection: 'row',
  },
  bellIcon: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
  },
  avatar: {
    width: 30,
    height: 30,
    borderRadius: 15,
    marginLeft: 15
  }
})
const mapStateToProps = state => ({
  auth: state.auth,
})
export default connect(mapStateToProps, null)(JobList)