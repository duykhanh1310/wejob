import React, { Component } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  Animated,
  Easing,
} from 'react-native'
import Icon from 'react-native-vector-icons/Entypo'
import IonIcon from 'react-native-vector-icons/Ionicons'
import MatIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import FastImage from 'react-native-fast-image'
import LottieView from 'lottie-react-native'

import Constant from '../../utils/Constant'
import Dispatch from '../../components/Dispatch'
import Type from '../../components/Type'

export default class JobDetail extends Component {
  constructor(props) {
    super(props)
    this.state = {
      progress: new Animated.Value(0),
      isShowFirework: false,
    }
  }

  _onSetupAnimation = () => {
    Animated.timing(this.state.progress, {
      toValue: 1,
      duration: 3000,
      easing: Easing.linear,
    }).start(() => {
      this.setState({ isShowFirework: false}, _ => {
        this.props.navigation.goBack()
      })
    })
  }
  
  _onRenderHeader() {
    const { code } = this.props.navigation.state.params.jobInfomation.item
    return (
      <View style={styles.headerContainer}>
        <View style={styles.leftSide}>
          <Icon
            name='chevron-small-left'
            size={30}
            color={Constant.colors.white}
            onPress={_ => this.props.navigation.goBack()}
          />
          <Text style={styles.jobCode}>{code}</Text>
        </View>
        <View style={styles.leftSide}>
          <MatIcon
            name='bell'
            color={Constant.colors.white}
            size={20}
            style={styles.bellIcon}
          />
          <Icon
            name='dots-three-horizontal'
            color={Constant.colors.white}
            size={20}
          />
        </View>
      </View>
    )
  }

  _onrenderBanner() {
    const { image } = this.props.navigation.state.params.jobInfomation.item
    return (
      <FastImage
        source={{ uri: image }}
        resizeMode={FastImage.resizeMode.cover}
        style={styles.bannerImg}
      />
    )
  }

  _onRenderTimeAndType() {
    const { address, type } = this.props.navigation.state.params.jobInfomation.item
    return (
      <View>
        <Text style={styles.jobDuration}>9:30am - 12:30pm</Text>
        <View style={styles.iConStack}>
          <Dispatch
            isDispatch={'Dispatched'}
          />
          <Type
            type={type}
          />
          <IonIcon
            name='ios-warning'
            color={Constant.colors.red}
            size={18}
            style={styles.warningIcon}
          />
        </View>
        <View style={styles.addresWrapper}>
          <Text
            numberOfLines={2}
            style={styles.address}
          >
            {`${address.street}, ${address.city}, ${address.cityCode}`}
          </Text>
          <IonIcon
            name='md-car'
            size={25}
            color={Constant.colors.lightGrey}
          />
        </View>
        
      </View>
    )
  }

  _onRenderDepartment() {
    const { department, contact } = this.props.navigation.state.params.jobInfomation.item
    return (
      <View style={{ marginTop: 15,}}>
        <Text style={styles.department}>{department}</Text>
        <View style={styles.contactContainer}>
          {/* Contact Person */}
          <View style={styles.contactWrapper}>
            <FastImage
              source={{ uri: contact.avatar }}
              style={styles.avatar}
              resizeMode={FastImage.resizeMode.contain}
            />
            <View style={styles.contactNameWrapper}>
              <Text style={styles.contactTitle}>Contact</Text>
              <Text style={styles.contactName}>{contact.name}</Text>
            </View>
          </View>
          {/* Contact Method */}
          <View style={styles.contactMethodWrapper}>
            <MatIcon
              name='comment-text'
              size={23}
              color={Constant.colors.lightGrey}
              style={{ marginRight: 10 }}
            />
            <MatIcon
              name='phone'
              size={23}
              color={Constant.colors.lightGrey}
            />
          </View>
        </View>
      </View>
    )
  }

  _onRenderDescription() {
    const { description } = this.props.navigation.state.params.jobInfomation.item
    return (
      <View style={styles.descriptionWrapper}>
        <Text style={styles.descriptionTitle}>Description</Text>
        <Text style={styles.description}>{description}</Text>
      </View>
    )
  }
  
  _onRenderInstruction() {
    const { instruction } = this.props.navigation.state.params.jobInfomation.item
    return (
      <View style={styles.descriptionWrapper}>
        <Text style={styles.descriptionTitle}>Special Instruction</Text>
        <Text style={styles.description}>{instruction}</Text>
      </View>
    )
  }

  _onRenderReaction() {
    return (
      <View style={styles.actionContainer}>
        <TouchableOpacity
          style={styles.declineWrapper}
          onPress={_ => this.props.navigation.goBack()}
        >
          <Text style={styles.decline}>Decline Jobs</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.acceptWrapper}
          onPress={_ => {
            this.setState({
              isShowFirework: true
            })
            this._onSetupAnimation()
          }}
        >
          <Text style={styles.accept}>Accept Jobs</Text>
        </TouchableOpacity>
      </View>
    )
  }

  _onRenderFireWork() {
    const { isShowFirework } = this.state
    if (isShowFirework) {
      return (
        <LottieView
          source={require('../../assets/Lottie/success.json')}
          progress={this.state.progress}
          autoPlay
          style={styles.firework}
        />
      )
    }
  }
  render() {
    return (
      <View style={{ flex: 1}}>
        <ScrollView style={styles.container}>
          {/* HEADER */}
          {this._onRenderHeader()}
          {/* BANNER */}
          {this._onrenderBanner()}
          <View style={styles.descriptionDetail}>
            {/* TIME TYPE */}
            {this._onRenderTimeAndType()}
            {/* DEPARTMENT */}
            {this._onRenderDepartment()}
            {/* DESCRIPTION */}
            {this._onRenderDescription()}
            {/* SPECTIAL INSTRUCTION */}
            {this._onRenderInstruction()}
            {/* REACTION TO JOBS */}
            {this._onRenderReaction()}
          </View>
          {this._onRenderFireWork()}
        </ScrollView>
        
      </View>
      
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  headerContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    backgroundColor: Constant.colors.primary,
    height: 70,
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: 10
  },
  leftSide: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  jobCode: {
    color: Constant.colors.white,
    marginLeft: 15,
    fontSize: 16,
    fontWeight: '600',
    alignSelf: 'center'
  },
  bellIcon: {
    marginLeft: 10,
    marginRight: 10
  },
  bannerImg: {
    width: Constant.layout.screenWidth,
    height: 180
  },
  jobDuration: {
    marginBottom: 10,
    fontWeight: '600',
    fontSize: 15,
    color: Constant.colors.lightGrey_2
  },
  descriptionDetail: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
  },
  warningIcon: {
    marginTop: 10,
    marginHorizontal: 15
  },
  iConStack: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  address: {
    color: Constant.colors.lightGrey_2,
    width: '70%',
    marginTop: 20,
    fontSize: 15,
    fontWeight: '500',
    letterSpacing: 0.3,
  },
  addresWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderStyle: 'solid',
    borderBottomWidth: 0.4,
    borderBottomColor: Constant.colors.lightGrey,
    paddingBottom: 15
  },
  department: { 
    marginBottom: 10,
    fontWeight: '600',
    color: Constant.colors.lightGrey_2,
    fontSize: 15,
    letterSpacing: 0.3,
  },
  contactContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderStyle: 'solid',
    borderBottomWidth: 0.4,
    borderBottomColor: Constant.colors.lightGrey,
    paddingBottom: 20,
  },
  contactWrapper: {
    display: 'flex',
    flexDirection: 'row'
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 20,
  },
  contactNameWrapper: {
    marginLeft: 10,
    justifyContent: 'center'
  },
  contactTitle: {
    color: Constant.colors.lightGrey,
    marginBottom: 5,
  },
  contactName: {
    color: Constant.colors.lightGrey_2,
    fontWeight: '600',
    fontSize: 15,
  },
  contactMethodWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  descriptionWrapper: {
    borderStyle: 'solid',
    borderBottomWidth: 0.4,
    borderBottomColor: Constant.colors.lightGrey,
    paddingBottom: 20,
    marginTop: 15,
  },  
  descriptionTitle: {
    color: Constant.colors.lightGrey,
    marginBottom: 15,
  },
  description: {
    color: Constant.colors.lightGrey_2,
    fontWeight: '500',
    letterSpacing: 0.2,
  },
  actionContainer: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 30,
  },
  declineWrapper: {
    borderStyle: 'solid',
    borderColor: Constant.colors.primary,
    borderWidth: 2,
    borderRadius: 5,
    width: '45%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 20,
  },
  decline: {
    color: Constant.colors.primary,
    fontSize: 17,
    fontWeight: '500'
  },
  acceptWrapper: {
    borderStyle: 'solid',
    borderColor: Constant.colors.primary,
    borderWidth: 2,
    borderRadius: 5,
    width: '45%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Constant.colors.primary,
  },
  accept: {
    color: Constant.colors.white,
    fontSize: 17,
    fontWeight: '500'
  },
  firework: {
    height: '50%',
    width: '100%',
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100
    // alignSelf: 'center',
  },
})