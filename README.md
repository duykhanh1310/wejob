# WeJob

# How to run
1. npm install or yarn install to get all packages ready
2. cd ios && pod install to install native module for iOS
3. react-native run-ios OR react-native run-android to run project on
iOS or Android respectively. I recommend using Xcode or Android Studio to
get the best result.

# e2e test
- If your machine doesn't have detox yet, please follow this link: https://github.com/wix/Detox/blob/master/docs/Introduction.GettingStarted.md
- Run detox test to test.

# What I done and my thought
- Implement Job List and Job Detail with full feature, run well on both iOS and Android. 
Because there are 2 pages only, I do not use redux-saga for 2 pages.

- To perfom redux-saga in this project, I integrate Facebook and Google authentication with firebase. 
But in the real world app like this, I think redux-saga isn't neccessary because of its complexity. I recommend
using redux-immutable instead. Redux-immutable is very powerful but simple to use.

- Due to lack of the remote API, I use fake data to generate fake data. All data in the Job card is used
for demonstration only.

# My weakness
- Testing is not my strength, though I realize its importance. In this project, I use detox to perfom e2e
method to test some basic components visible.
- I will improve my testing skill to adapt with future project.

